
function drawGraph($container_id=null, $type=null) {
	$.ajax({
		method: "GET",
		url: "graph/get",
		data: {
			graph_type: $type,
			element_type: "vertices",
			action_type: "get",
			response_type: "array"
		},
		success: function(data) {
			vertices_edges = [];
			$.each(data, function( index, value ) {
				vertices_edges.push({ data: { id: index} })
			});
			$.each(data, function(index, object ) {
				$.each(object, function(i, value) {
					vertices_edges.push({ data: { id: index + "-" + i, source: index.toString(), target: i.toString()} })
				})
			});

			graph = cytoscape({
				container: document.getElementById($container_id),
				elements: vertices_edges,
				style: [
					{
						selector: 'node',
						style: {
							'background-color': '#666',
							'label': 'data(id)'
						}
					},
					{
						selector: 'edge',
						style: {
							'width': 3,
							'line-color': '#ccc',
							'target-arrow-color': '#ccc',
							'target-arrow-shape': 'triangle'
						}
					}
				],
				layout: {
					name: 'cose'
				}
			});

			graph.on('tap', 'node', function(evt){
				var node = evt.cyTarget;
				fillVertexEdit(node.id());
			});

			graph.on('tap', 'edge', function(evt){
				var edge = evt.cyTarget;
				fillEdgeEdit(edge.id());
			});
		}
	});
}

function fillVertexEdit($vertex_name) {
	$.ajax({
		method: "GET",
		url: "graph/get",
		data: {
			graph_type: "full",
			element_type: "vertex",
			action_type: "get",
			vertex_name: $vertex_name
		},
		success: function(data) {
			var form = $("#add-vertex-form");
			var delete_form = $("#delete-vertex-form");
			$.each(data, function(k, v) {
				if(k == "neighbours") {
					var $i = 0;
					var $neighbours_quantity = Object.keys(v).length;
					$.each(v, function(neighbour_id, weight) {
						if($i != 0) form.find("select[name='vertex-neighbour-" + ($i-1) + "']").trigger("change", { neighbour_id: neighbour_id });
						if($i === 0) {
							if($neighbours_quantity === 1) {
								form.find("select[name='vertex-neighbour-0']").trigger("change").val(neighbour_id);
								return;
							}
							form.find("select[name='vertex-neighbour-" + $i + "']").val(neighbour_id);
						}							
						form.find("input[name='vertex-weight-" + $i + "']").val(weight);
						if($i == $neighbours_quantity-1) form.find("select[name='vertex-neighbour-" + ($i-1) + "']").trigger("change");
						
						$i++;
					});
					return;
				}
				form.find("input[name='vertex-" + k + "']").val(v);
				if(k === "id") delete_form.find("select[name='vertex-name']").val(v);
			});
		}
	});
}

function fillEdgeEdit($edge_name) {
	$.ajax({
		method: "GET",
		url: "graph/get",
		data: {
			graph_type: "full",
			element_type: "edge",
			action_type: "get",
			edge_name: $edge_name
		},
		success: function(data) {
			var form = $("#add-edge-form");
			var delete_form = $("#delete-edge-form");
			$.each(data, function(k, v) {
				if(k === "vertex_a_id" || k === "vertex_b_id") {
					k_array = k.split("_");					
					form.find("select[name='edge-vertex-" + k_array[1] + "']").val(v);
					delete_form.find("select[name='edge-vertex-" + k_array[1] + "']").val(v);
					return;
				}
				form.find("input[name='edge-" + k + "']").val(v);
			});
		}
	});
}

function fillSelect(select=null, value=null) {
	$.ajax({
		method: "GET",
		url: "graph/get",
		data: {
			graph_type: "full",
			element_type: "vertices",
			action_type: "get",
			response_type: "collection"
		},
		success: function(data) {
			var option = document.createElement("OPTION");
			option.text = "new";
			select.add(option); 
			$.each(data, function(index, object) {
				var option = document.createElement("OPTION");
				option.text = object.name; 
				option.value = object.id;
				select.add(option); 
			});
			if(value) {
				select.value = value.neighbour_id;
			}
		}
	});
}

function displayMessage(message, type) {
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-top-center",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	switch(type) {
		case "success":
			toastr.success(message);
			break;
		case "error":
			toastr.error(message);
			break;
		case "warning":
			toastr.warning(message);
			break;
		default:
			toastr.info(message);
			break;
	}
}

function formAnimation(form=False, object=null) {
	object.find("i").toggleClass('fa-arrow-circle-down fa-arrow-circle-up');
	if(form.css('display') == 'none') {
		form.fadeIn();
	} else {
		form.fadeOut();
	}
}

function clearVertexInputs() {
	$("#add-vertex-neighbours").find("option").remove();
	fillSelect($("#add-vertex-neighbours")["0"]);
	$("#delete-vertex").find("option").remove();
	fillSelect($("#delete-vertex")["0"]);
	$("#add-vertex-neighbours").nextAll().remove();
	$("[name=vertex-weight-0]").val("").nextAll().remove();
	$("#vertex-name").val("");
	$("#vertex-level").val("");
	$("#vertex-status").val("");
}

function clearEdgeInputs() {
	$("#add-edge-vertex-a").find("option").remove();
	fillSelect($("#add-edge-vertex-a")["0"]);
	$("#add-edge-vertex-b").find("option").remove();
	fillSelect($("#add-edge-vertex-b")["0"]);
	$("#edge-weight").val("");
	$("#edge-status").val("");
	$("#delete-edge-vertex-a").find("option").remove();
	fillSelect($("#delete-edge-vertex-a")["0"]);
	$("#delete-edge-vertex-b").find("option").remove();
	fillSelect($("#delete-edge-vertex-b")["0"]);
}

function clearInputs() {
	clearVertexInputs();
	clearEdgeInputs();
}

function initializePage() {
	graph;
	drawGraph('graph', $('#graph-level-options').find(":selected").data("type"));
	fillSelect($("#add-vertex-neighbours")["0"]);
	fillSelect($("#add-edge-vertex-a")["0"]);
	fillSelect($("#add-edge-vertex-b")["0"]);
	fillSelect($("#delete-edge-vertex-a")["0"]);
	fillSelect($("#delete-edge-vertex-b")["0"]);
	fillSelect($("#delete-vertex")["0"]);
}

$(document).ready(function() {
//init
	initializePage()

//event for dynamic added content
	$('#graph-level-options').on("change", function(e){
		$("select option:selected").each(function() {
			if($(this).data("type")) {
				graph.destroy();
				drawGraph('graph', $(this).data("type"));
			}
		});
	});

	$(document).on("change", ".add-vertex-neighbours", function(e, neighbour_id){
	//select: vertex-neighobur-0
		$(this).nextAll().remove();
		var select = document.createElement("SELECT");
		fillSelect(select, neighbour_id);
		select.className = "form-control add-vertex-neighbours";
		var $tmp = $(this)["0"].name.split("-");
		var $tmp_count = $tmp.length;
		select.name = "vertex-" + $tmp[$tmp_count-2] + "-" + (parseInt($tmp[$tmp_count-1])+1);
	//input: vertex-weight-0
		$("[name=vertex-weight-" + $tmp[$tmp_count-1] + "]").nextAll().remove();
		var input = document.createElement("INPUT");
		input.className = "form-control";
		input.name = "vertex-weight-" + (parseInt($tmp[$tmp_count-1])+1);
		input.setAttribute("placeholder", "weight");

		if($(this).attr("name").search("edge") < 0) {
			$(this).parent().append(select);
			$("[name=vertex-weight-" + $tmp[$tmp_count-1] + "]").parent().append(input);
		}
	});
	
	$("#add-vertex-form-toggle").on('click', function(e){
		formAnimation(form = $("#add-vertex-form"), object = $(this));
	});
	$("#add-edge-form-toggle").on('click', function(e){
		formAnimation(form = $("#add-edge-form"), object = $(this));
	});
	$("#delete-vertex-form-toggle").on('click', function(e){
		formAnimation(form = $("#delete-vertex-form"), object = $(this));
	});
	$("#delete-edge-form-toggle").on('click', function(e){
		formAnimation(form = $("#delete-edge-form"), object = $(this));
	});

//change action type in form on click button
	$(".graph-form-button").on("click", function(event) {
		$(this).closest("form").data("action-type", $(this).attr("value").toLowerCase());
	});	

//Send forms AJAX
	$(document).on("submit", ".send-ajax-form", function(event){
		event.preventDefault();
		var form = $(this);
		$.ajax({
			method: form.attr("method"),
			url: form.attr("action"),
			data: {
				_token: $('meta[name="csrf-token"]').attr('content'),
				graph_type: form.data("graph-type"),
				element_type: form.data("element-type"),
				action_type: form.data("action-type"),
				form: form.serialize()
			},
			success: function(data) {
				displayMessage(message=data["message"], type=data["type"]);
				drawGraph('graph', $('#graph-level-options').find(":selected").data("type"));
				if((form.data("element-type") == "vertex") || (form.data("element-type") == "vertices")) clearVertexInputs();
				if((form.data("element-type") == "edge") || (form.data("element-type") == "edges")) clearEdgeInputs();
			}
		});
	});	
});