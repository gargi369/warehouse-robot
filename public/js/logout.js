$("#logout").click(function(event) {
	event.preventDefault();
	$.ajax({
		method: "POST",
		url: "logout",
		data: {
			_token: $('meta[name="csrf-token"]').attr('content')
		},
		success: function(data){
			window.location = "login"
		}
	});
});