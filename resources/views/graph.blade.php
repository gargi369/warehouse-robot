@extends("root")

@section("title")
	<title>Warehouse Robot - graph</title>
@endsection

@section("styles")
	{!! Html::style("css/graph.css") !!}
@endsection

@section("content")
	<div class="container">
		<div class="row">
			<div id="menu" class="col-md-3">
				<div class="form-group">
					<label for="graph-level-options">Graph level:</label>
					<select class="form-control" id="graph-level-options">
						<option data-type="main">Main</option>
						<option data-type="helper">With helper vertices</option>
						<option data-type="full">Full</option>
						<option data-type="path">Path</option>
					</select>
				</div>
				<hr>

				{{-- add vertex --}}
				<div>
					<label id="add-vertex-form-toggle" data-toggle="collapse" data-target="#add-vertex-form" for="add-vertex-form"><i class="collapse-left fa fa-arrow-circle-down"></i>Add/Edit vertex:</label>
					{!! Form::open([
						"url" => route("graph.post"), 
						"id" => "add-vertex-form", 
						"class" => "form-horizontal collapse send-ajax-form", 
						"method" => "POST",
						"data-graph-type" => "full",
						"data-element-type" => "vertex",
						"data-action-type" => "add"
					]) !!}
						{{-- vertex id --}}
						<div class="form-group">
							<div class="col-sm-12">
								{{ Form::hidden(
									'vertex-id', 
									'', 
									[
										"class" => "form-control",
										"placeholder" => "id"
									]) 
								}}								
							</div>
						</div>
						{{-- vertex name --}}
						<div class="form-group">
							{{ Form::label(
								"vertex-name", 
								"name", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::text(
									'vertex-name', 
									'', 
									[
										"class" => "form-control",
										"placeholder" => "name"
									]) 
								}}								
							</div>
						</div>
						{{-- vertex level --}}
						<div class="form-group">
							{{ Form::label(
								"vertex-level", 
								"level", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::text(
									'vertex-level', 
									'', 
									[
										"class" => "form-control",
										"placeholder" => "level"
									]) 
								}}								
							</div>
						</div>
						{{-- vertex status --}}
						<div class="form-group">
							{{ Form::label(
								"vertex-status", 
								"status", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::text(
									'vertex-status', 
									'', 
									[
										"class" => "form-control",
										"placeholder" => "status"
									]) 
								}}								
							</div>
						</div>
						{{-- neighbours --}}
						<div id="add-vertex-neighbours-div" class="form-group vertex-neighbours-group">
							{{ Form::label(
								"neighbours-div", 
								"neighbours", 
								[
									"class" => "col-sm-12 control-label"
								])
							}}
							<div id="neighbours-div">
								<div class="col-sm-4 vertex-neighbours">
									{{ Form::text(
										'vertex-weight-0', 
										'', 
										[
											"class" => "form-control",
											"placeholder" => "weight"
										]) 
									}}
								</div>
								<div class="col-sm-8 vertex-neighbours">
									{{ Form::select(
										'vertex-neighbour-0', 
										[],
										null, 
										[
											"class" => "form-control add-vertex-neighbours",
											"id" => "add-vertex-neighbours"
										]) 
									}}
								</div>
							</div>
						</div>
						<div class="form-group form-horizontal-button">
							{{ Form::submit(
								'Add',
								[
									"class" => "btn btn-info graph-form-button"
								]) 
							}}
							{{ Form::submit(
								'Edit',
								[
									"class" => "btn btn-warning graph-form-button"
								]) 
							}}
						</div>
					{!! Form::close() !!}
				</div>
				<hr>
				{{-- delete vertex --}}
				<div>
					<label id="delete-vertex-form-toggle" data-toggle="collapse" data-target="#delete-vertex-form" for="delete-vertex-form"><i class="collapse-left fa fa-arrow-circle-down"></i>Delete vertex:</label>

					{!! Form::open([
						"url" => route("graph.post"), 
						"id" => "delete-vertex-form", 
						"class" => "form-horizontal collapse send-ajax-form", 
						"method" => "POST",
						"data-graph-type" => "full",
						"data-element-type" => "vertex",
						"data-action-type" => "delete"
					]) !!}
						{{-- vertex --}}
						<div class="form-group">
							{{ Form::label(
								"vertex-name", 
								"vertex", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::select(
									'vertex-name', 
									[],
									null, 
									[
										"class" => "form-control",
										"id" => "delete-vertex"
									]) 
								}}								
							</div>
						</div>
						<div class="form-group form-horizontal-button">
							{{ Form::submit(
								'Delete',
								[
									"class" => "btn btn-danger graph-form-button"
								]) 
							}}
						</div>
					{!! Form::close() !!}
				</div>
				<hr>

				{{-- add edge --}}
				<div>
					<label id="add-edge-form-toggle" data-toggle="collapse" data-target="#add-edge-form" for="add-edge-form"><i class="collapse-left fa fa-arrow-circle-down"></i>Add/Edit edge:</label>

					{!! Form::open([
						"url" => route("graph.post"), 
						"id" => "add-edge-form", 
						"class" => "form-horizontal collapse send-ajax-form", 
						"method" => "POST",
						"data-graph-type" => "full",
						"data-element-type" => "edge",
						"data-action-type" => "add"
					]) !!}
						{{-- edge id --}}
						<div class="form-group">
							<div class="col-sm-12">
								{{ Form::hidden(
									'edge-id', 
									'', 
									[
										"class" => "form-control",
										"placeholder" => "id"
									]) 
								}}								
							</div>
						</div>
						{{-- vertex-a --}}
						<div class="form-group">
							{{ Form::label(
								"edge-vertex-a", 
								"vertex-a", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::select(
									'edge-vertex-a', 
									[],
									null, 
									[
										"class" => "form-control add-vertex-neighbours",
										"id" => "add-edge-vertex-a"
									]) 
								}}								
							</div>
						</div>
						{{-- vertex-b --}}
						<div class="form-group">
							{{ Form::label(
								"edge-vertex-b", 
								"vertex-b", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::select(
									'edge-vertex-b', 
									[],
									null, 
									[
										"class" => "form-control add-vertex-neighbours",
										"id" => "add-edge-vertex-b"
									]) 
								}}								
							</div>
						</div>
						{{-- weight --}}
						<div id="add-edge-neighbours" class="form-group">
							{{ Form::label(
								"edge-weight", 
								"weight", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::number(
									'edge-weight', 
									'weight', 
									[
										"class" => "form-control",
										"placeholder" => "status"
									]) 
								}}								
							</div>
						</div>
						{{-- status --}}
						<div id="add-edge-neighbours" class="form-group">
							{{ Form::label(
								"edge-status", 
								"status", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::number(
									'edge-status', 
									'status', 
									[
										"class" => "form-control",
										"placeholder" => "status"
									]) 
								}}								
							</div>
						</div>
						<div class="form-group form-horizontal-button">
							{{ Form::submit(
								'Add',
								[
									"class" => "btn btn-info graph-form-button"
								]) 
							}}
							{{ Form::submit(
								'Edit',
								[
									"class" => "btn btn-warning graph-form-button"
								]) 
							}}
						</div>
					{!! Form::close() !!}
				</div>
				<hr>

				{{-- delete edge --}}
				<div>
					<label id="delete-edge-form-toggle" data-toggle="collapse" data-target="#delete-edge-form" for="delete-edge-form"><i class="collapse-left fa fa-arrow-circle-down"></i>Delete edge:</label>

					{!! Form::open([
						"url" => route("graph.post"), 
						"id" => "delete-edge-form", 
						"class" => "form-horizontal collapse send-ajax-form", 
						"method" => "POST",
						"data-graph-type" => "full",
						"data-element-type" => "edge",
						"data-action-type" => "delete"
					]) !!}
						{{-- vertex-a --}}
						<div class="form-group">
							{{ Form::label(
								"delete-edge-vertex-a", 
								"vertex-a", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::select(
									'edge-vertex-a', 
									[],
									null, 
									[
										"class" => "form-control",
										"id" => "delete-edge-vertex-a"
									]) 
								}}								
							</div>
						</div>
						{{-- vertex-b --}}
						<div class="form-group">
							{{ Form::label(
								"delete-edge-vertex-b", 
								"vertex-b", 
								[
									"class" => "col-sm-4 control-label"
								])
							}}
							<div class="col-sm-8">
								{{ Form::select(
									'edge-vertex-b', 
									[],
									null, 
									[
										"class" => "form-control",
										"id" => "delete-edge-vertex-b"
									]) 
								}}								
							</div>
						</div>
						<div class="form-group form-horizontal-button">
							{{ Form::submit(
								'Delete',
								[
									"class" => "btn btn-danger graph-form-button"
								]) 
							}}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			<div id="graph" class="col-md-9"></div>
		</div>
	</div>
@endsection

@section("scripts")
	@parent
	{!! Html::script("js/vendor/cytoscape.min.js") !!}
	{!! Html::script("js/graph.js") !!}
@endsection