@extends("root")

@section("title")
	<title>Warehouse Robot - home</title>
@endsection

@section("content")
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Home</div>

					<div class="panel-body">
						Welcome in home.
						Change your world!
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
