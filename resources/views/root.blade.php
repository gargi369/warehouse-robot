<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Krzysztof Gardziejewski">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	@yield("title")

	{!! Html::style("css/vendor/bootstrap.min.css") !!}
	{!! Html::style("css/vendor/font-awesome.min.css") !!}
	{!! Html::style("//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.css") !!}
	@yield("styles")

</head>

<body>

	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ route("main") }}">Warehouse Robot</a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ route("home") }}">Home</a></li>
					<li><a href="{{ route("graph.show") }}">Graph</a></li>
					<li><a href="{{ route("graph.show") }}">Order</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url("/login") }}">Login</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								{{ Auth::user()->name }} <span class="caret"></span>
							</a>

							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url("/logout") }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield("content")

	{!! Html::script("js/vendor/jquery.min.js") !!}
	{!! Html::script("js/vendor/bootstrap.min.js") !!}
	{!! Html::script("//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.js") !!}
	@yield("scripts")

</body>

</html>
