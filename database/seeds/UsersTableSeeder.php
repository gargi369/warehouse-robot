<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	public function run() {
		$users = [
			["name" => "gargi", "password" => Hash::make("test"), "email" => "testLast@test.pl"],
			["name" => "teasdawdwast", "password" => Hash::make("test"), "email" => "testLast2@test.pl"],
			["name" => "qwdwqdqw", "password" => Hash::make("test"), "email" => "testLast3@test.pl"]
		];

		foreach($users as $user) {
			User::firstOrCreate($user);
		}
	}
}
