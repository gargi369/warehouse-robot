<?php

use App\Models\Vertex;
use Illuminate\Database\Seeder;

class VerticesTableSeeder extends Seeder {

	public function run() {
		$vertices = [
			// main
			["name" => "1", "level" => 100],
			["name" => "2", "level" => 100],
			["name" => "3", "level" => 100],
			["name" => "4", "level" => 100],
			["name" => "5", "level" => 100],
			["name" => "6", "level" => 100],
			["name" => "7", "level" => 100],
				["name" => "7|-1|1|1", "level" => 300],
				["name" => "7|-1|2|1", "level" => 300],				

			//helpers
			// 1-5
			["name" => "1|5|1", "level" => 200],
				["name" => "1|5|1|1", "level" => 300],
				["name" => "1|5|1|2", "level" => 300],
			["name" => "1|5|2", "level" => 200],
				["name" => "1|5|2|1", "level" => 300],
				["name" => "1|5|2|2", "level" => 300],
			["name" => "1|5|3", "level" => 200],
				["name" => "1|5|3|1", "level" => 300],
				["name" => "1|5|3|2", "level" => 300],
			["name" => "1|5|4", "level" => 200],
				["name" => "1|5|4|1", "level" => 300],
				["name" => "1|5|4|2", "level" => 300],
			["name" => "1|5|5", "level" => 200],
				["name" => "1|5|5|1", "level" => 300],
				["name" => "1|5|5|2", "level" => 300],

			// 1-2
			["name" => "1|2|1", "level" => 200],
				["name" => "1|2|1|1", "level" => 300],
				["name" => "1|2|1|2", "level" => 300],
			["name" => "1|2|2", "level" => 200],
				["name" => "1|2|2|1", "level" => 300],
				["name" => "1|2|2|2", "level" => 300],
			["name" => "1|2|3", "level" => 200],
				["name" => "1|2|3|1", "level" => 300],
				["name" => "1|2|3|2", "level" => 300],
			["name" => "1|2|4", "level" => 200],
				["name" => "1|2|4|1", "level" => 300],
				["name" => "1|2|4|2", "level" => 300],

			// 1-4
			["name" => "1|4|1", "level" => 200],
				["name" => "1|4|1|1", "level" => 300],
				["name" => "1|4|1|2", "level" => 300],

			// 2-3
			["name" => "2|3|1", "level" => 200],
				["name" => "2|3|1|1", "level" => 300],
				["name" => "2|3|1|2", "level" => 300],
			["name" => "2|3|2", "level" => 200],
				["name" => "2|3|2|1", "level" => 300],
				["name" => "2|3|2|2", "level" => 300],
			["name" => "2|3|3", "level" => 200],
				["name" => "2|3|3|1", "level" => 300],
				["name" => "2|3|3|2", "level" => 300],

			// 4-5
			["name" => "4|5|1", "level" => 200],
				["name" => "4|5|1|1", "level" => 300],
				["name" => "4|5|1|2", "level" => 300],
			["name" => "4|5|2", "level" => 200],
				["name" => "4|5|2|1", "level" => 300],
				["name" => "4|5|2|2", "level" => 300],

			// 6-7
			["name" => "6|7|1", "level" => 200],
				["name" => "6|7|1|1", "level" => 300],
				["name" => "6|7|1|2", "level" => 300],
			["name" => "6|7|2", "level" => 200],
				["name" => "6|7|2|1", "level" => 300],

			// 5-7
			["name" => "5|7|1", "level" => 200],
				["name" => "5|7|1|1", "level" => 300],
				["name" => "5|7|1|2", "level" => 300],
			["name" => "5|7|2", "level" => 200],
				["name" => "5|7|2|1", "level" => 300],
			["name" => "5|7|3", "level" => 200],
				["name" => "5|7|3|1", "level" => 300],

		];

		foreach($vertices as $vertex) {
			Vertex::firstOrCreate($vertex);
		}
	}
}
