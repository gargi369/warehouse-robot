<?php

use App\Models\Edge;
use Illuminate\Database\Seeder;

class EdgesTableSeeder extends Seeder {

	public function run() {
		$edges = [
			["vertex_a_id" => 1, "vertex_b_id" => 2, "weight" => 625],
			["vertex_a_id" => 1, "vertex_b_id" => 4, "weight" => 320],
			["vertex_a_id" => 1, "vertex_b_id" => 5, "weight" => 1250],
			["vertex_a_id" => 1, "vertex_b_id" => 25, "weight" => 105],
			["vertex_a_id" => 1, "vertex_b_id" => 37, "weight" => 200],
			["vertex_a_id" => 1, "vertex_b_id" => 10, "weight" => 325],

			["vertex_a_id" => 2, "vertex_b_id" => 1, "weight" => 625],
			["vertex_a_id" => 2, "vertex_b_id" => 3, "weight" => 449],
			["vertex_a_id" => 2, "vertex_b_id" => 34, "weight" => 30],
			["vertex_a_id" => 2, "vertex_b_id" => 40, "weight" => 264],

			["vertex_a_id" => 3, "vertex_b_id" => 2, "weight" => 449],
			["vertex_a_id" => 3, "vertex_b_id" => 4, "weight" => 808],
			["vertex_a_id" => 3, "vertex_b_id" => 46, "weight" => 85],

			["vertex_a_id" => 4, "vertex_b_id" => 1, "weight" => 320],
			["vertex_a_id" => 4, "vertex_b_id" => 3, "weight" => 808],
			["vertex_a_id" => 4, "vertex_b_id" => 5, "weight" => 269],
			["vertex_a_id" => 4, "vertex_b_id" => 37, "weight" => 120],
			["vertex_a_id" => 4, "vertex_b_id" => 49, "weight" => 97],

			["vertex_a_id" => 5, "vertex_b_id" => 1, "weight" => 1250],
			["vertex_a_id" => 5, "vertex_b_id" => 4, "weight" => 269],
			["vertex_a_id" => 5, "vertex_b_id" => 6, "weight" => 153],
			["vertex_a_id" => 5, "vertex_b_id" => 7, "weight" => 666],
			["vertex_a_id" => 5, "vertex_b_id" => 22, "weight" => 211],
			["vertex_a_id" => 5, "vertex_b_id" => 52, "weight" => 98],
			["vertex_a_id" => 5, "vertex_b_id" => 60, "weight" => 171],

			["vertex_a_id" => 6, "vertex_b_id" => 5, "weight" => 153],
			["vertex_a_id" => 6, "vertex_b_id" => 7, "weight" => 150],
			["vertex_a_id" => 6, "vertex_b_id" => 55, "weight" => 50],

			["vertex_a_id" => 7, "vertex_b_id" => 5, "weight" => 666],
			["vertex_a_id" => 7, "vertex_b_id" => 6, "weight" => 150],
			["vertex_a_id" => 7, "vertex_b_id" => 65, "weight" => 160],
			["vertex_a_id" => 7, "vertex_b_id" => 58, "weight" => 50],
			["vertex_a_id" => 7, "vertex_b_id" => 8],
			["vertex_a_id" => 7, "vertex_b_id" => 9],

			["vertex_a_id" => 25, "vertex_b_id" => 1, "weight" => 105],
			["vertex_a_id" => 25, "vertex_b_id" => 28, "weight" => 117],
			["vertex_a_id" => 25, "vertex_b_id" => 26],
			["vertex_a_id" => 25, "vertex_b_id" => 27],

			["vertex_a_id" => 28, "vertex_b_id" => 25, "weight" => 117],
			["vertex_a_id" => 28, "vertex_b_id" => 31, "weight" => 259],
			["vertex_a_id" => 28, "vertex_b_id" => 29],
			["vertex_a_id" => 28, "vertex_b_id" => 30],

			["vertex_a_id" => 31, "vertex_b_id" => 28, "weight" => 259],
			["vertex_a_id" => 31, "vertex_b_id" => 34, "weight" => 141],
			["vertex_a_id" => 31, "vertex_b_id" => 32],
			["vertex_a_id" => 31, "vertex_b_id" => 33],

			["vertex_a_id" => 34, "vertex_b_id" => 31, "weight" => 141],
			["vertex_a_id" => 34, "vertex_b_id" => 2, "weight" => 30],
			["vertex_a_id" => 34, "vertex_b_id" => 35],
			["vertex_a_id" => 34, "vertex_b_id" => 36],

			["vertex_a_id" => 40, "vertex_b_id" => 2, "weight" => 264],
			["vertex_a_id" => 40, "vertex_b_id" => 43, "weight" => 50],
			["vertex_a_id" => 40, "vertex_b_id" => 41],
			["vertex_a_id" => 40, "vertex_b_id" => 42],

			["vertex_a_id" => 43, "vertex_b_id" => 40, "weight" => 50],
			["vertex_a_id" => 43, "vertex_b_id" => 46, "weight" => 50],
			["vertex_a_id" => 43, "vertex_b_id" => 44],
			["vertex_a_id" => 43, "vertex_b_id" => 45],

			["vertex_a_id" => 46, "vertex_b_id" => 43, "weight" => 50],
			["vertex_a_id" => 46, "vertex_b_id" => 3, "weight" => 85],
			["vertex_a_id" => 46, "vertex_b_id" => 47],
			["vertex_a_id" => 46, "vertex_b_id" => 48],

			["vertex_a_id" => 37, "vertex_b_id" => 1, "weight" => 200],
			["vertex_a_id" => 37, "vertex_b_id" => 4, "weight" => 120],
			["vertex_a_id" => 37, "vertex_b_id" => 38],
			["vertex_a_id" => 37, "vertex_b_id" => 39],

			["vertex_a_id" => 49, "vertex_b_id" => 4, "weight" => 97],
			["vertex_a_id" => 49, "vertex_b_id" => 52, "weight" => 74],
			["vertex_a_id" => 49, "vertex_b_id" => 50],
			["vertex_a_id" => 49, "vertex_b_id" => 51],

			["vertex_a_id" => 52, "vertex_b_id" => 49, "weight" => 74],
			["vertex_a_id" => 52, "vertex_b_id" => 5, "weight" => 98],
			["vertex_a_id" => 52, "vertex_b_id" => 53],
			["vertex_a_id" => 52, "vertex_b_id" => 54],

			["vertex_a_id" => 10, "vertex_b_id" => 1, "weight" => 325],
			["vertex_a_id" => 10, "vertex_b_id" => 13, "weight" => 203],
			["vertex_a_id" => 10, "vertex_b_id" => 11],
			["vertex_a_id" => 10, "vertex_b_id" => 12],

			["vertex_a_id" => 13, "vertex_b_id" => 10, "weight" => 203],
			["vertex_a_id" => 13, "vertex_b_id" => 16, "weight" => 166],
			["vertex_a_id" => 13, "vertex_b_id" => 14],
			["vertex_a_id" => 13, "vertex_b_id" => 15],

			["vertex_a_id" => 16, "vertex_b_id" => 13, "weight" => 166],
			["vertex_a_id" => 16, "vertex_b_id" => 19, "weight" => 165],
			["vertex_a_id" => 16, "vertex_b_id" => 17],
			["vertex_a_id" => 16, "vertex_b_id" => 18],

			["vertex_a_id" => 19, "vertex_b_id" => 16, "weight" => 165],
			["vertex_a_id" => 19, "vertex_b_id" => 22, "weight" => 180],
			["vertex_a_id" => 19, "vertex_b_id" => 20],
			["vertex_a_id" => 19, "vertex_b_id" => 21],

			["vertex_a_id" => 22, "vertex_b_id" => 19, "weight" => 18],
			["vertex_a_id" => 22, "vertex_b_id" => 5, "weight" => 211],
			["vertex_a_id" => 22, "vertex_b_id" => 23],
			["vertex_a_id" => 22, "vertex_b_id" => 24],

			["vertex_a_id" => 55, "vertex_b_id" => 6, "weight" => 50],
			["vertex_a_id" => 55, "vertex_b_id" => 58, "weight" => 50],
			["vertex_a_id" => 55, "vertex_b_id" => 56],
			["vertex_a_id" => 55, "vertex_b_id" => 57],

			["vertex_a_id" => 58, "vertex_b_id" => 55, "weight" => 50],
			["vertex_a_id" => 58, "vertex_b_id" => 7, "weight" => 50],
			["vertex_a_id" => 58, "vertex_b_id" => 59],

			["vertex_a_id" => 60, "vertex_b_id" => 5, "weight" => 171],
			["vertex_a_id" => 60, "vertex_b_id" => 63, "weight" => 163],
			["vertex_a_id" => 60, "vertex_b_id" => 61],
			["vertex_a_id" => 60, "vertex_b_id" => 62],

			["vertex_a_id" => 63, "vertex_b_id" => 60, "weight" => 163],
			["vertex_a_id" => 63, "vertex_b_id" => 65, "weight" => 172],
			["vertex_a_id" => 63, "vertex_b_id" => 64],

			["vertex_a_id" => 65, "vertex_b_id" => 63, "weight" => 172],
			["vertex_a_id" => 65, "vertex_b_id" => 7, "weight" => 160],
			["vertex_a_id" => 65, "vertex_b_id" => 66],

			["vertex_a_id" => 26, "vertex_b_id" => 25],
			["vertex_a_id" => 27, "vertex_b_id" => 25],
			["vertex_a_id" => 29, "vertex_b_id" => 28],
			["vertex_a_id" => 30, "vertex_b_id" => 28],
			["vertex_a_id" => 32, "vertex_b_id" => 31],
			["vertex_a_id" => 33, "vertex_b_id" => 31],
			["vertex_a_id" => 35, "vertex_b_id" => 34],
			["vertex_a_id" => 36, "vertex_b_id" => 34],
			["vertex_a_id" => 38, "vertex_b_id" => 37],
			["vertex_a_id" => 39, "vertex_b_id" => 37],
			["vertex_a_id" => 11, "vertex_b_id" => 10],
			["vertex_a_id" => 12, "vertex_b_id" => 10],
			["vertex_a_id" => 14, "vertex_b_id" => 13],
			["vertex_a_id" => 15, "vertex_b_id" => 13],
			["vertex_a_id" => 17, "vertex_b_id" => 16],
			["vertex_a_id" => 18, "vertex_b_id" => 16],
			["vertex_a_id" => 20, "vertex_b_id" => 19],
			["vertex_a_id" => 21, "vertex_b_id" => 19],
			["vertex_a_id" => 23, "vertex_b_id" => 22],
			["vertex_a_id" => 24, "vertex_b_id" => 22],
			["vertex_a_id" => 41, "vertex_b_id" => 40],
			["vertex_a_id" => 42, "vertex_b_id" => 40],
			["vertex_a_id" => 44, "vertex_b_id" => 43],
			["vertex_a_id" => 45, "vertex_b_id" => 43],
			["vertex_a_id" => 47, "vertex_b_id" => 46],
			["vertex_a_id" => 48, "vertex_b_id" => 46],
			["vertex_a_id" => 50, "vertex_b_id" => 49],
			["vertex_a_id" => 51, "vertex_b_id" => 49],
			["vertex_a_id" => 53, "vertex_b_id" => 52],
			["vertex_a_id" => 54, "vertex_b_id" => 52],
			["vertex_a_id" => 61, "vertex_b_id" => 60],
			["vertex_a_id" => 62, "vertex_b_id" => 60],
			["vertex_a_id" => 64, "vertex_b_id" => 63],
			["vertex_a_id" => 66, "vertex_b_id" => 65],
			["vertex_a_id" => 56, "vertex_b_id" => 55],
			["vertex_a_id" => 57, "vertex_b_id" => 55],
			["vertex_a_id" => 59, "vertex_b_id" => 58],
			["vertex_a_id" => 8, "vertex_b_id" => 7],
			["vertex_a_id" => 9, "vertex_b_id" => 7]
		];

		foreach($edges as $edge) {
			Edge::firstOrCreate($edge);
		}
	}
}
