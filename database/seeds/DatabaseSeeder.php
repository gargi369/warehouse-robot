<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	public function run() {
		// Userzy
		$this->command->info("Creating sample users...");
		$this->call(UsersTableSeeder::class);

		// Vertices
		$this->command->info("Creating sample vertices...");
		$this->call(VerticesTableSeeder::class);

		// Edges
		$this->command->info("Creating sample edges...");
		$this->call(EdgesTableSeeder::class);
	}

}
