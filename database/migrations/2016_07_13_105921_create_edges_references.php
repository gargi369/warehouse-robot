<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdgesReferences extends Migration {

	public function up() {
		Schema::table("edges", function($table) {
            $table->foreign("vertex_a_id")->references("id")->on("vertices");
            $table->foreign("vertex_b_id")->references("id")->on("vertices");
        });
	}

	public function down() {
	}
}
