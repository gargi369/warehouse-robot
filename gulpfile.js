/*!
 * gulp
 * $ npm install gulp-ruby-sass gulp-autoprefixer gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

// Load plugins
var gulp = require('gulp'),
		sass = require('gulp-ruby-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		cssnano = require('gulp-cssnano'),
		jshint = require('gulp-jshint'),
		uglify = require('gulp-uglify'),
		imagemin = require('gulp-imagemin'),
		rename = require('gulp-rename'),
		concat = require('gulp-concat'),
		notify = require('gulp-notify'),
		cache = require('gulp-cache'),
		livereload = require('gulp-livereload'),
		del = require('del');

// Styles
gulp.task('styles', function() {
	return sass('resources/assets/sass/*.scss', { style: 'expanded' })
		.pipe(autoprefixer('last 2 version'))
		.pipe(gulp.dest('public/css'))
		.pipe(rename({ suffix: '.min' }))
		.pipe(cssnano())
		.pipe(gulp.dest('public/css'));
});

// Scripts
// gulp.task('scripts', function() {
// 	return gulp.src('resources/assets/js/*.js')
// 		.pipe(jshint('.jshintrc'))
// 		.pipe(jshint.reporter('default'))
// 		.pipe(gulp.dest('public/js'))
// 		.pipe(rename({ suffix: '.min' }))
// 		.pipe(uglify())
// 		.pipe(gulp.dest('public/js'))
// 		.pipe(notify({ message: 'Scripts task complete' }));
// });
gulp.task('scripts', function() {
	gulp.src('./resources/assets/js/*.js')
		.pipe(gulp.dest('./public/js'));
});

// Images
// gulp.task('images', function() {
// 	return gulp.src('src/images/**/*')
// 		.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
// 		.pipe(gulp.dest('dist/images'))
// 		.pipe(notify({ message: 'Images task complete' }));
// });

//bootstrap
gulp.task('bootstrap_js', function() {
	gulp.src('resources/bower/bootstrap/dist/css/bootstrap.min.js')
		.pipe(gulp.dest('./public/css/vendor'));
});
gulp.task('bootstrap_css', function() {
	gulp.src('resources/bower/bootstrap/dist/js/bootstrap.min.css')
		.pipe(gulp.dest('./public/js/vendor'));
});
gulp.task('bootstrap_fonts', function() {
	gulp.src('resources/bower/bootstrap/dist/fonts/*')
		.pipe(gulp.dest('./public/fonts'));
});
gulp.task('bootstrap', function() {
	gulp.start('bootstrap_css', 'bootstrap_js', 'bootstrap_fonts');
});

//cytoscape
gulp.task('cytoscape_js', function() {
	gulp.src('resources/bower/cytoscape/dist/cytoscape.min.js')
		.pipe(gulp.dest('./public/js/vendor'));
});
gulp.task('cytoscape', function() {
	gulp.start('cytoscape_js');
});

//toastr
gulp.task('toastr_js', function() {
	gulp.src('resources/bower/toastr/toastr.min.js')
		.pipe(gulp.dest('./public/js/vendor'));
});
gulp.task('toastr_css', function() {
	gulp.src('resources/bower/toastr/toastr.min.css')
		.pipe(gulp.dest('./public/js/vendor'));
});
gulp.task('toastr', function() {
	gulp.start('toastr_js', 'toastr_css');
});

//jquery
gulp.task('jquery_js', function() {
	gulp.src('resources/bower/jquery/dist/jquery.min.js')
		.pipe(gulp.dest('./public/js/vendor'));
});
gulp.task('jquery', function() {
	gulp.start('jquery_js');
});

//fontawesome !!!! CHANGE path on: ../../fonts !!!!
gulp.task('fontawesome_css', function() {
	return sass('resources/bower/font-awesome/scss/font-awesome.scss', { style: 'expanded' })
		.pipe(autoprefixer('last 2 version'))
		.pipe(rename({ suffix: '.min' }))
		.pipe(cssnano())
		.pipe(gulp.dest('./public/css/vendor'));
});
gulp.task('fontawesome_fonts', function() {
	gulp.src('resources/bower/font-awesome/fonts/*')
		.pipe(gulp.dest('./public/fonts'));
});
gulp.task('fontawesome', function() {
	gulp.start('fontawesome_fonts', 'fontawesome_css');
});



// Default task
gulp.task('default', function() {
	gulp.start('styles', 'scripts'); //, 'images'
});

// Watch
gulp.task('watch', function() {

	// Watch .scss files
	gulp.watch('resources/assets/sass/**/*.scss', ['styles']);

	// Watch .js files
	gulp.watch('resources/assets/js/**/*.js', ['scripts']);

	// Watch image files
	// gulp.watch('src/images/**/*', ['images']);

	// Create LiveReload server
	livereload.listen();

	// Watch any files in dist/, reload on change
	gulp.watch(['assets/**']).on('change', livereload.changed);

});