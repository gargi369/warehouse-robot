<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public function getHome() {
        return view('home');
    }

    public function getMain() {
        return view('main');
    }
}
