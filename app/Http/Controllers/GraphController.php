<?php

namespace App\Http\Controllers;

use App\Helpers\Graph;

use View, Input;
use Illuminate\Http\Request;

class GraphController extends MainController {

	protected $graph_main;
	protected $graph_path;
	protected $graph_helper;
	protected $graph_full;

	protected $graph_kinds = [
		"main" => 100,
		"path" => 100,
		"helper" => 200,
		"full" => 300
	];

	public function __construct() {
		$this->fillActionsAndElements();
	}

	public function fillActionsAndElements() {
		$this->elements = [
			"vertex",
			"vertices",
			"edge",
			"edges",
			"graph",
			"graphs"
		];

		$this->actions = [
			"add" => "post",
			"edit" => "post",
			"delete" => "post",
			"get" => "get",
			"show" => "get"
		];
	}

//check wanted graph object
	protected function checkGraphObject($request) {
		if(array_has($this->graph_kinds, $request->input("graph_type"))) {
			$graph_name = "graph_" . $request->input("graph_type");
			if(!isset($this->{$graph_name}))
				$graph = $this->{$graph_name} = new Graph($this->graph_kinds[$request->input("graph_type")]);

			return $graph_name;
		}

		return false;
	}

// handle GET request from graph()
	protected function getVertices($request) {
		$method_main_name = $this->methodParser($request);

		if($method_main_name) {
			$response_type = $method_main_name . ucfirst($request->input("response_type"));
			$graph_name = $this->checkGraphObject($request);

			return $this->$graph_name->{$response_type}();
		}

		return response()->json([
				"message" => "Method from request for graph doesn't exist.",
				"type" => "error"
			]);	
	}

	protected function getVertex($request) {
		$graph_name = $this->checkGraphObject($request);
		$method_main_name = $this->methodParser($request);

		return $this->{$graph_name}->{$method_main_name}($request->input("vertex_name"));
	}

	protected function getEdge($request) {
		$graph_name = $this->checkGraphObject($request);
		$method_main_name = $this->methodParser($request);

		return $this->{$graph_name}->{$method_main_name}(explode("-", $request->input("edge_name")));
	}

// handle POST request from graph()
	protected function graphMethodParserForForm($request) {
		$graph_name = $this->checkGraphObject($request);

		if($graph_name) {
			$form_array = [];
			parse_str($request->input("form"), $form_array);
			$form_array = array_except($form_array, ["_token"]);

			return $this->{$graph_name}->{$request->input("action_type") . ucfirst($request->input("element_type"))}($form_array);
		}

		return response()->json([
				"message" => "Method from request for graph doesn't exist.",
				"type" => "error"
			]);	
	}

	protected function addVertex($request) {
		return $this->graphMethodParserForForm($request);
	}

	protected function editVertex($request) {
		return $this->graphMethodParserForForm($request);
	}

	protected function deleteVertex($request) {
		return $this->graphMethodParserForForm($request);
	}

	protected function addEdge($request) {
		return $this->graphMethodParserForForm($request);
	}

	protected function editEdge($request) {
		return $this->graphMethodParserForForm($request);
	}

	protected function deleteEdge($request) {
		return $this->graphMethodParserForForm($request);
	}

	public function showGraph() {
		return View::make("graph");
	}
}
