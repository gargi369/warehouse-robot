<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Interfaces\Mainable;

abstract class MainController extends Controller implements Mainable {

	protected $actions;
	protected $elements;

// check and create proper method name
	protected function methodParser($request) {
		$action = $request->input("action_type");
		$element = $request->input("element_type");
		if($request->isMethod('get'))
			$request_type = 'get';
		if($request->isMethod('post'))
			$request_type = 'post';

		if(array_has($this->actions, $action) 
			&& in_array($element, $this->elements) 
			&& $this->actions[$action] == $request_type) {
			
			return $action . ucfirst($element);
		}

		return false;
	}

// handle request for controller
	public function requestHandle(Request $request) {
		$method_name = $this->methodParser($request);

		if($method_name) {
			return $this->{$method_name}($request);
		} else {
			return response()->json([
					"message" => "Method from request doesn't exist.",
					"type" => "error"
				]);	
		}
	}

}
