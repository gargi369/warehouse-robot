<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get("/", "HomeController@getMain")->name("main");

Route::group(["middleware" => ["auth"]], function() {

	Route::get("/home", "HomeController@getHome")->name("home");

	Route::get("/graph", "GraphController@showGraph")->name("graph.show");
	Route::post("/graph/post", "GraphController@requestHandle")->name("graph.post");
	Route::get("/graph/get", "GraphController@requestHandle")->name("graph.get");		
});

Route::auth();
