<?php

namespace App\Models;

use App\Models\Vertex;
use Illuminate\Database\Eloquent\Model;

class Edge extends Model
{
    protected $fillable = [ 
    	"vertex_a",
    	"vertex_b",
    	"weight",
    	"status"
	];

	public $timestamps = false;

	public function vertex_a() {
		return $this->belongsTo(Vertex::class);
	}

	public function vertex_b() {
		return $this->belongsTo(Vertex::class);
	}
}
