<?php

namespace App\Models;

use App\Models\Edge;
use Illuminate\Database\Eloquent\Model;

class Vertex extends Model
{
	public $timestamps = false;
	
    protected $fillable = [ 
    	"name",
    	"level",
    	"status"
	];

	public function edges() {
		return $this->hasMany(Edge::class, "vertex_a_id");
	}

}
