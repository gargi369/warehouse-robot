<?php

namespace App\Helpers;

use App\Models\Vertex;
use App\Models\Edge;
use Illuminate\Http\Request;

class Graph {

	protected $vertices = [];
	protected $level;

	public function __construct($level=NULL){
		if(!is_null($level)) {
			$this->buildGraph($level);
		}
	}

//handle build graph 
	public function buildGraph($level) {
		$vertices = Vertex::where("level", "<=",$level)->get();
		$this->level = $level;
		foreach($vertices as $vertex) {
			$this->addVertex($vertex);
		}

		return $this;
	}

//handle edge
//add
	public function addEdge($edge) {
		switch(gettype($edge)) {
			case "array":
				return $this->addEdgeFromArray($edge);
				break;
		}
	}
	protected function addEdgeFromArray($edge_array, $double=true) {
		try {
			$edge = new Edge();
			$edge->vertex_a_id = $edge_array["edge-vertex-a"];
			$edge->vertex_b_id = $edge_array["edge-vertex-b"];
			$edge->weight = $edge_array["edge-weight"];
			if(isset($edge_array["edge-status"])) $edge->status = $edge_array["edge-status"];
			$edge->save();

			if($double) {
				$edge = new Edge();
				$edge->vertex_a_id = $edge_array["edge-vertex-b"];
				$edge->vertex_b_id = $edge_array["edge-vertex-a"];
				$edge->weight = $edge_array["edge-weight"];
				if(isset($edge_array["edge-status"])) $edge->status = $edge_array["edge-status"];
				$edge->save();
			}

			return response()->json([
					"message" => "Successfully added edge.",
					"type" => "success"
				]);	
		} catch (Exception $e) {
			return response()->json([
					"message" => "Adding edge has error. Check data in inputs.",
					"type" => "error"
				]);
		}
	}
//edit
	public function editEdge($edge) {
		switch(gettype($edge)) {
			case "array":
				return $this->editEdgeFromArray($edge);
				break;
			case "object" && str_contains(get_class($edge), "Edge"):
				return $this->editEdgeFromObject($edge);
				break;
		}
	}
	protected function editEdgeFromArray($edge_array, $double=true) {	
		try {
			$edge = Edge::where("id", $edge_array["edge-id"])->first();

			return $this->editEdgeFromObject($edge, $edge_array);
		} catch (Exception $e) {
			return response()->json([
					"message" => "Adding edge has error. Check data in inputs.",
					"type" => "error"
				]);	
		}
	}
	protected function editEdgeFromObject($edge, $edge_array, $double=true) {	
		try {
			$edge->vertex_a_id = $edge_array["edge-vertex-a"];
			$edge->vertex_b_id = $edge_array["edge-vertex-b"];
			$edge->weight = $edge_array["edge-weight"];
			$edge->status = $edge_array["edge-status"];
			$edge->save();

			if($double) {
				$edge_mirror = Edge::where([
						["vertex_a_id", $edge->vertex_b_id],
						["vertex_b_id", $edge->vertex_a_id]
					])->first();
				$edge_mirror->vertex_a_id = $edge_array["edge-vertex-b"];
				$edge_mirror->vertex_b_id = $edge_array["edge-vertex-a"];
				$edge_mirror->weight = $edge_array["edge-weight"];
				$edge_mirror->status = $edge_array["edge-status"];
				$edge_mirror->save();
			}

			return response()->json([
					"message" => "Successfully edited edge.",
					"type" => "success"
				]);	
		} catch (Exception $e) {
			return response()->json([
					"message" => "Adding edge has error. Check data in inputs.",
					"type" => "error"
				]);	
		}
	}
//delete
	public function deleteEdge($edge){
		switch(gettype($edge)) {
			case "array":
				return $this->deleteEdgeFromArray($edge);
				break;
			case "object" && str_contains(get_class($edge), "Edge"):
				return $this->deleteEdgeFromMarginalVertices($edge->vertex_a, $edge->vertex_b);
				break;
		}
	}

	protected function deleteEdgeFromArray($edge_array) {
		if(sizeof($edge_array) == 2) {
			$vertex_a = Vertex::find($edge_array["edge-vertex-a"]);
			$vertex_b = Vertex::find($edge_array["edge-vertex-b"]);

			return $this->deleteEdgeFromMarginalVertices($vertex_a, $vertex_b);
		}
	}

	protected function deleteEdgeFromMarginalVertices(Vertex $vertex_a, Vertex $vertex_b, $double=true) {
		foreach($vertex_a->edges as $edge) {
			if($edge->vertex_b_id == $vertex_b->id)
				$edge->delete();		
		}
		if($double) {
			foreach($vertex_b->edges as $edge) {
				if($edge->vertex_b_id == $vertex_a->id)
					$edge->delete();		
			}
		}

		return response()->json([
				"message" => "Successfully deleted edge.",
				"type" => "success"
			]);	
	}

//handle vertex
//add
	public function addVertex($vertex) {
		switch(gettype($vertex)) {
			case "array":
				return $this->addVertexFromArray($vertex);
				break;
			case "object" && str_contains(get_class($vertex), "Vertex"):
				return $this->addVertexFromObject($vertex);
				break;
		}
	}

	protected function addVertexFromArray($vertex_array) {
		$vertex = new Vertex();
		$condition = "neighbour";
		try {
			$vertex->name = $vertex_array["vertex-name"];
			$vertex->level = $vertex_array["vertex-level"];
			$vertex->status = $vertex_array["vertex-status"];
			$vertex->save();
			$this->addVertexFromObject($vertex);

			foreach($vertex_array as $key => $value) {
				if(str_contains($key, $condition) && $value != "new") {
					$this->addEdgeFromArray([
						"edge-vertex-a" => $vertex->id,
						"edge-vertex-b" => $value,
						"edge-weight" => $vertex_array["vertex-weight-" . last(explode("-", $key))]
					]);
				}
			}

			return response()->json([
					"message" => "Successfully added vertex.",
					"type" => "success"
				]);	
		} catch (Exception $e) {
			return response()->json([
					"message" => "Adding vertex has error. Check data in inputs.",
					"type" => "error"
				]);
		}
	}

	protected function addVertexFromObject($vertex_object) {
		array_push($this->vertices, $vertex_object);
	}

//edit

//delete
	public function deleteVertex($vertex) {
		switch(gettype($vertex)) {
			case "array":
				return $this->deleteVertexFromArray($vertex);
				break;
			case "object" && str_contains(get_class($vertex), "Vertex"):
				return $this->deleteVertexFromObject($vertex);
				break;
		}
	}

	protected function deleteVertexFromArray($vertex_array) {
		if(isset($vertex_array["vertex-name"])) {
			$vertex = Vertex::find($vertex_array["vertex-name"]);

			return $this->deleteVertexFromObject($vertex);
		}

		return false;
	}

	protected function deleteVertexFromObject(Vertex $vertex) {
		try {
			foreach($vertex->edges as $edge) {
				foreach($edge->vertex_b->edges as $neighbour_edge) {
					if($neighbour_edge->vertex_b->id == $vertex->id) {
						$neighbour_edge->delete();
					}
				}
				$edge->delete();
			}
			$vertex->delete();

			return response()->json([
				"message" => "Successfully deleted vertex.",
				"type" => "success"
				]);	
		} catch (Exception $e) {
			return false;
		}
	}

//handle getting vertices and vertex
	public function getVertex($vertex_name) {
		$vertex = Vertex::where("name", $vertex_name)->first();
		if($vertex) {
			$vertex_attribiutes_array = $vertex->getAttributes();
			$neighbours_array = [];

			foreach($vertex->edges as $edge) {
				$neighbours_array = array_add($neighbours_array, $edge->vertex_b_id, $edge->weight);
			}
			$vertex_attribiutes_array = array_add($vertex_attribiutes_array, "neighbours" , $neighbours_array);

			return $vertex_attribiutes_array;
		}

		return "006";
	}

	public function getEdge($vertices_names) {
		$vertex = Vertex::where("name", $vertices_names[0])->first();

		if($vertex) {
			foreach($vertex->edges as $edge) {
				if($edge->vertex_b->name == $vertices_names[1]) {
					return $edge->getAttributes();
				}
			}
		}

		return "006";
	}

	public function getVerticesCollection() {
		return $this->vertices;
	}

	public function getVerticesArray() {
		$vertices_array = [];
		foreach($this->vertices as $vertex) {
			$edges_array = [];
			foreach($vertex->edges as $edge) {
				if($edge->vertex_b->level <= $this->level)
					$edges_array = array_add($edges_array, $edge->vertex_b->name, $edge->weight);
			}
			$vertices_array = array_add($vertices_array, (string)$vertex->name, $edges_array);
		}

		return $vertices_array;
	}

//handle finding shortest path
	public function shortest_path_Dijkstra($vertex_begin, $vertex_end) {

	}

}
